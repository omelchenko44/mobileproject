import i18n from 'i18n-js';
import en_EN from './en';
import ru_RU from './ru';
import uk_UK from './uk';
import {NativeModules, Platform} from "react-native";

i18n.defaultLocale = 'en_EN';
let locale = Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
i18n.locale = locale;
i18n.fallbacks = true;
i18n.translations = {en_EN, ru_RU, uk_UK};

export default i18n;
