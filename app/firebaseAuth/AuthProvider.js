import React, {createContext, useState} from "react";
import auth from "@react-native-firebase/auth";
import clog from "../Utils/Functions/clog";
import i18n from "i18n-js";

const {t} = i18n;
export const AuthContext = createContext();
export const AuthProvider = ({children}) => {
    const [user, setUser] = useState();

    return (
        <AuthContext.Provider
            value={{
                user,
                setUser,
                login: async (email, password, showingAlert) => {
                    try {
                        await auth().signInWithEmailAndPassword(email, password)
                    } catch (err) {
                        clog(err)
                        clog('err in signInWithEmailAndPassword')
                        if (err.toString().includes('user-not-found')) {
                            showingAlert(t('userNotFound'));
                        } else if (err.toString().includes('wrong-password')) {
                            showingAlert(t('wrongPassword'));
                        } else {
                            showingAlert(t('pleaseTryLater'));
                        }
                    }
                },
                register: async (email, password, showingAlert) => {
                    try {
                        await auth().createUserWithEmailAndPassword(email, password);
                    } catch (err) {
                        clog(err);
                        clog('err in createUserWithEmailAndPassword');
                        if (err.toString().includes('weak-password')) {
                            showingAlert(t('userAlreadyExist'));
                        } else if (err.toString().includes('email-already-in-use')) {
                            showingAlert(t('userAlreadyExist'));
                        } else {
                            showingAlert(t('pleaseTryLater'));
                        }
                    }
                },
                loginWithGoogle: async (credentials) => {
                    try {
                        await auth().signInWithCredential(credentials);
                    } catch (err) {
                        clog(err)
                        clog('err in signInWithCredential')
                    }
                },
                logout: async (email, password) => {
                    try {
                        await auth().signOut(email, password)
                    } catch (err) {
                        clog(err)
                        clog('err in signOut')
                    }
                },
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};
