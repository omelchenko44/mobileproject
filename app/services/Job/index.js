import ajax from '../../lib/ajax';
import clog from "../../Utils/Functions/clog";
import {districtActions} from "../../storage/realm";

export default class Job {
    async getDistrict() {
        let controller = new AbortController();
        return ajax('https://decentralization.gov.ua/graphql?query={areas{title,id,square,population,local_community_count,percent_communities_from_area,sum_communities_square}}', {controller})
            .then((data) => {
                districtActions.saveDistrictList(data?.['data']?.['areas'] ?? []);
            })
            .catch((err) => {
                clog("Catch in job getDistrict")
                clog(err)
            })
    }

}
