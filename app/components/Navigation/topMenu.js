import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export const TopMenu = ({navigationProps, navigationBack, name}) => {

    function toggleDrawer() {
        navigationProps.openDrawer();
    }

    function goBack() {
        navigationBack.goBack();
    }

    return (
        <View style={styles.container}>
            {
                navigationProps &&
                <TouchableOpacity onPress={() => {
                    toggleDrawer()
                }}>
                    <Icon name={"menu"} style={styles.icon} size={35}/>
                </TouchableOpacity>
            }
            {
                navigationBack &&
                <TouchableOpacity onPress={() => {
                    goBack()
                }}>
                    <Icon name={"arrow-left"} style={styles.icon} size={30}/>
                </TouchableOpacity>
            }
            {
                <View>
                    <Text style={styles.headerText}>{name}</Text>
                </View>
            }
        </View>
    );
}

const styles = StyleSheet.create({
        container: {
            flexDirection: 'row',
            backgroundColor: "white",
            elevation: 10,
            alignItems: 'center',
            height: 50,
            width: '100%'
        },
        icon: {
            marginLeft: 10
        },
        headerText: {
            fontSize: 20,
            marginLeft: 5,
            marginBottom: 3,
            fontWeight: "bold",
        },
    }
)
