import React, {useEffect, useState} from "react";
import {NavigationContainer} from "@react-navigation/native";
import {SignIn} from "../../screen/login/signIn";
import {CreateAccount} from "../../screen/login/createAcount";
import auth from "@react-native-firebase/auth";
import {createNativeStackNavigator} from "react-native-screens/native-stack";
import {Main} from "../../screen/Main";
import {Camera} from "../Camera";

const Stack = createNativeStackNavigator();
export const Navigation = () => {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();

    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);
    }

    useEffect(() => {
        return auth().onAuthStateChanged(onAuthStateChanged);
    }, []);

    if (initializing) return null;
    return (
        <NavigationContainer>
            {user ?
                <Stack.Navigator initialRouteName="Main">
                    <Stack.Screen name="Main"
                                  component={Main}
                                  initialParams={{user: user}}
                                  options={{headerShown: false}}
                    />
                    <Stack.Screen name="Camera"
                                  component={Camera}
                                  options={{headerShown: false}}
                    />
                </Stack.Navigator>
                :
                <Stack.Navigator initialRouteName="SignIn">
                    <Stack.Screen name="SignIn"
                                  component={SignIn}
                                  options={{headerShown: false}}
                    />
                    <Stack.Screen name="CreateAccount"
                                  component={CreateAccount}
                                  options={{headerShown: false}}/>
                </Stack.Navigator>

            }
        </NavigationContainer>
    );
}
