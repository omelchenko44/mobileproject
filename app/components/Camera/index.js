import React, {useRef, useState} from "react";
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import styled from 'styled-components';
import {RNCamera} from 'react-native-camera';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const flashIcons = {
    'on': <Image source={require('../../assets/cameraIcon/flashOn.png')} style={{width: 50, height: 50}}/>,
    'auto': <Image source={require('../../assets/cameraIcon/flashAuto.png')} style={{width: 50, height: 50}}/>,
    'off': <Image source={require('../../assets/cameraIcon/flashOff.png')} style={{width: 50, height: 50}}/>,
}
const constRotateCamera = {
    'back': <Image source={require('../../assets/cameraIcon/rotate.png')} style={{width: 45, height: 45}}/>,
    'front': <Image source={require('../../assets/cameraIcon/rotate.png')} style={{width: 45, height: 45}}/>,
}

export const Camera = ({route, navigation}) => {
    const {onMakePhoto} = route.params;
    const cameraRef = useRef();

    const [locationX, setLocationX] = useState(0);
    const [locationY, setLocationY] = useState(0);
    const [coord_x, setCoord_x] = useState(0);
    const [coord_y, setCoord_y] = useState(0);
    const [flashMode, setFlashMode] = useState('off');
    const [cameraType, setCameraType] = useState('back');
    const [zoom] = useState(0);


    function rotateCamera() {
        if (cameraType === 'back') {
            setCameraType('front');
        } else if (cameraType === 'front') {
            setCameraType('back');
        }
    }

    function toggleFlash() {
        if (flashMode === 'off') {
            setFlashMode('auto');
        } else if (flashMode === 'auto') {
            setFlashMode('on');
        } else if (flashMode === 'on') {
            setFlashMode('off');
        }
    }

    function focus(e) {
        const absoluteX = e.nativeEvent.locationX;
        const absoluteY = e.nativeEvent.locationY;
        const coord_x = e.nativeEvent.pageX;
        const coord_y = e.nativeEvent.pageY;

        const isPortrait = screenHeight > screenWidth;
        let x = absoluteX / screenWidth;
        let y = absoluteY / screenHeight;
        if (isPortrait) {
            x = absoluteX / screenHeight;
            y = -(absoluteX / screenWidth) + 1;
        }
        setLocationX(x);
        setLocationY(y);
        setCoord_x(coord_x);
        setCoord_y(coord_y);
        setTimeout(() => {
            setCoord_x(null);
        }, 2000);
    }

    async function takePicture(onMakePhoto, closeCamera, navigation) {
        if (cameraRef.current) {
            const options = {
                fixOrientation: true,
                forceUpOrientation: true,
                quality: 0.4,
            };
            const data = await cameraRef.current.takePictureAsync(options);
            if (closeCamera) {
                navigation.goBack()
                onMakePhoto(data, true);
            } else {
                onMakePhoto(data, false);
            }
        }
    }

    return (
        <Root onPress={focus}>
            <RNCamera
                ref={cameraRef}
                style={styles.preview}
                autoFocus={RNCamera.Constants.AutoFocus.on}
                autoFocusPointOfInterest={{x: locationX, y: locationY}}
                flashMode={flashMode}
                type={cameraType}
                zoom={zoom}
                maxZoom={0.9}
                useNativeZoom={true}
                onTap={focus}
                captureAudio={false}>
                {coord_x ?
                    <View style={{
                        borderWidth: 3,
                        borderColor: 'white',
                        position: 'absolute',
                        top: coord_y - 75,
                        left: coord_x - 31,
                        borderRadius: 60,
                        width: 60,
                        height: 60
                    }}>
                    </View>
                    : null
                }
                <View style={styles.buttonView}>
                    <TouchableOpacity onPress={() => {
                        navigation.goBack();
                    }}>
                        <Text style={{
                            left: 10,
                            bottom: 10,
                            fontSize: 45,
                            width: 50,
                            height: 50,
                            color: "white"
                        }}>
                            ×
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.photoView}>
                    <FlashButton onPress={toggleFlash}>
                        {flashIcons[flashMode]}
                    </FlashButton>
                    <TouchableOpacity onPress={() => {
                        takePicture(onMakePhoto, true, navigation).then(() => {
                        });
                    }}>
                        <Photo source={require('../../assets/cameraIcon/circle.png')}/>
                    </TouchableOpacity>
                    <RotateButton onPress={rotateCamera}>
                        {constRotateCamera[cameraType]}
                    </RotateButton>
                </View>
            </RNCamera>
        </Root>
    );
}

const styles = StyleSheet.create({
    preview: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    buttonView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    photoView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    }
});

const Photo = styled(Image)`
  bottom: 15px;
  width: 110px;
  height: 110px;
`;

const FlashButton = styled(TouchableOpacity)`
  padding: 15px;
  margin: 20px;
  bottom: 10px;
`;

const Root = styled(TouchableOpacity)`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
`;

const RotateButton = styled(TouchableOpacity)`
  padding: 15px;
  margin: 20px;
  bottom: 14px
`;
