import {keyValueActions} from '../storage/realm/index';
import _ from "lodash";
import {TIMEOUT} from "../config";

export default function ajax(url, data, callback) {
    let headers = {};

    if (typeof data === 'function') {
        callback = data;
        data = {};
    }

    if (typeof callback === 'object') {
        headers = callback;
        callback = null;
    }
    let timeout = keyValueActions.getValue('timeout') ? parseInt(keyValueActions.getValue('timeout'), 10) : TIMEOUT;
    setTimeout(() => {
        try {
            !_.isEmpty(data.controller) ? data.controller.abort() : null
        } catch (err) {
            console.log(err)
            console.log('err in ajax')
        }

    }, timeout);

    let p = fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...headers
        },
        body: JSON.stringify(data || {}),
        signal: data.controller.signal,
    }).then(function (response) {
        if (response.ok) {
            return response.json().catch(function () {
                return response.text();
            });
        } else {
            throw response;
        }
    }, function (err) {

    })

    if (callback) {
        p.then(callback);
    }
    return p;
}
