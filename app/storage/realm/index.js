import Realm from "realm";

import type {KeyValueActionInterface} from "./actions/keyValueAction";
import keyValueAction from "./actions/keyValueAction";
import KeyValueModel from "./models/keyValueModel";

import type {DistrictActionInterface} from "./actions/districtAction";
import districtAction from "./actions/districtAction";
import DistrictModel from "./models/districtModel";


const realmInstance = new Realm({
    schema: [
        KeyValueModel,
        DistrictModel,
    ],
    //0.1 == 1
    schemaVersion: 1,
});


/**
 * Available keyValue action
 * @type {KeyValueActionInterface}
 */
export const keyValueActions: KeyValueActionInterface = keyValueAction(realmInstance);

/**
 *
 * @type {DistrictActionInterface|*}
 */
export const districtActions: DistrictActionInterface = districtAction(realmInstance);
