/**
 * District Model
 */
export default class DistrictModel {
    /**
     * Getter of the class
     * @return {string} class name
     */
    static getDistrictModelName() {
        return DistrictModel.schema.name;
    }

    /**
     * class {realm} schema
     * @type {{name: string, properties: {value: string, key: string}}}
     */
    static schema = {
        'name': 'district',
        'primaryKey': '_id',

        'properties': {
            '_id': 'int',
            'name': 'string',
        },
    }
}

/**
 * Model Flow type
 * @type {Object}
 */
export type DistrictModelTypeInterface = {
    _id: number,
    name: string
}
