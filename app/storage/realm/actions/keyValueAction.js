import _ from 'lodash';
import KeyValueModel from '../models/keyValueModel';
import type {KeyValueModelTypeInterface} from '../models/keyValueModel';

/**
 * Flow type of KeyValueAction
 * @type {Object}
 */
export type KeyValueActionInterface = {
    setRegion(region: any): Promise<KeyValueModel>,
    getRegion(): any,
    getValue(key: string): String,
    setValue(key: string, value: string): Promise<KeyValueModel>,
}

/**
 *
 * @param realmInstance
 * @returns {{getValue: function(string): String, setRegion: function(*): Promise<KeyValueModel>, getRegionObject: function(): KeyValueModelTypeInterface, getRegion: function(): String, setValue: function(string, string): Promise<KeyValueModel>, getValueObject: function(string): KeyValueModelTypeInterface}}
 */
export default (realmInstance: any): KeyValueActionInterface => {
     let obj = {
         /**
          *
          * @param region
          * @returns {Promise<KeyValueModel>}
          */
         setRegion: (region: any): Promise<KeyValueModel> => {
             return new Promise((resolve) => {
                 try {
                     let regionRecord = obj.getRegionObject();
                     if (!_.isEmpty(regionRecord)){
                         realmInstance.write(() => {
                             regionRecord[0].value = JSON.stringify(region);
                         });
                         resolve();
                     }
                     else {
                         const keyValue: KeyValueModelTypeInterface = {
                             key: "region",
                             value: JSON.stringify(region),
                         };
                         realmInstance.write(() => {
                             const createdKeyValue = realmInstance.create(KeyValueModel.getKeyValueModelName(), keyValue, true);
                             resolve(createdKeyValue);
                         });
                     }
                 } catch (error) {
                     resolve(error);
                 }
             });
         },
         getRegionObject: (): KeyValueModelTypeInterface => {
             return realmInstance.objects(KeyValueModel.getKeyValueModelName()).filtered('key = "region"');
         },
         getRegion: (): String => {
             let regionObj = realmInstance.objects(KeyValueModel.getKeyValueModelName()).filtered('key = "region"');
             if (!_.isEmpty(regionObj)){
                 return JSON.parse(regionObj[0].value);
             } else{
                 return null;
             }
         },
         getValue: (key: string): String => {
             let getValue = realmInstance.objects(KeyValueModel.getKeyValueModelName()).filtered('key = "{key}"'.replace("{key}", key));
             if (getValue.length>0){
                 return getValue[0].value;
             } else{
                 return null;
             }
         },
         getValueObject: (key: string): KeyValueModelTypeInterface => {
             return realmInstance.objects(KeyValueModel.getKeyValueModelName()).filtered('key = "{key}"'.replace('{key}', key));
         },

         setValue: (key: string, value: string): Promise<KeyValueModel> => {
             return new Promise((resolve) => {
                 try {
                     let setValue = obj.getValueObject(key);
                     if (!_.isEmpty(setValue)){
                         realmInstance.write(() => {
                             setValue[0].value = value;
                             resolve();
                         });
                     }
                     else {
                         const keyValue: KeyValueModelTypeInterface = {
                             key: key,
                             value: value,
                         };
                         realmInstance.write(() => {
                             const createdKeyValue = realmInstance.create(KeyValueModel.getKeyValueModelName(), keyValue, true);
                             resolve(createdKeyValue);
                         });
                     }
                 } catch (error) {
                     resolve(error);
                 }
             });
         },
    };
    return obj;
};
