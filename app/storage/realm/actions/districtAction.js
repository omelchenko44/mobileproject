import type {DistrictModelTypeInterface} from '../models/districtModel';
import DistrictModel from '../models/districtModel';

export type DistrictActionInterface = {
    saveDistrictList(districtList: DistrictModelTypeInterface): DistrictModelTypeInterface,
    getAllDistricts(): DistrictModelTypeInterface,
    getById(): DistrictModelTypeInterface,
    getByIdList(): DistrictModelTypeInterface,
    deleteAll(): void,
    deleteByIdList(districtList: DistrictModelTypeInterface): DistrictModelTypeInterface,
}

/**
 *
 * @param realmInstance
 * @returns {{getByIdList: (function(*): *), getAllDistricts: (function(): *), getById: (function(string): *), deleteByIdList: ((function(*): DistrictModelTypeInterface)|*), saveDistrictList: saveDistrictList, deleteAll: deleteAll}}
 */
export default (realmInstance: any): DistrictActionInterface => {
    return {
        /**
         *
         * @param districtList
         */
        saveDistrictList: (districtList: any) => {
            try {
                realmInstance.write(() => {
                    for (let district of districtList) {
                        const {id, title} = district;

                        realmInstance.create(DistrictModel.getDistrictModelName(), {
                            _id: id,
                            name: title,
                        }, true);
                    }
                })
            } catch (error) {
                console.log(error);
            }
        },
        /**
         *
         * @returns {*}
         */
        getAllDistricts: (): DistrictModelTypeInterface => {
            return realmInstance.objects(DistrictModel.getDistrictModelName());
        },
        /**
         *
         * @param id
         * @returns {Realm.Results<unknown>}
         */
        getById: (id: string): DistrictModelTypeInterface => {
            return realmInstance.objects(DistrictModel.getDistrictModelName()).filtered("_id=" + id);
        },
        /**
         *
         * @param idList
         * @returns {Realm.Results<unknown>}
         */
        getByIdList: (idList: any): DistrictModelTypeInterface => {
            return realmInstance.objects(DistrictModel.getDistrictModelName()).filtered(idList.map((id) => '_id == ' + id).join(' OR '));
        },
        deleteAll: () => {
            try {
                let all = DistrictAction.getAllDistricts();
                realmInstance.write(() => {
                    realmInstance.delete(all);
                });
            } catch (error) {
                console.log(error);
            }
        },
        /**
         *
         * @param idList
         */
        deleteByIdList: (idList: any): DistrictModelTypeInterface => {
            try {
                let deleted_list = realmInstance.objects(DistrictModel.getDistrictModelName()).filtered(idList.map((id) => '_id == ' + id).join(' OR '));
                realmInstance.write(() => {
                    realmInstance.delete(deleted_list);
                });
            } catch (error) {
                console.log(error);
            }
        }
    };
};
