import {DEBUG} from "../../config";

export default function clog(log) {
    if (DEBUG === true) {
        console.log(log);
    }
}
