import storage from "@react-native-firebase/storage";
import clog from "../Functions/clog";

export default function uploadPhotoToFirebase(fileUri, id) {
    const fileName = fileUri?.split('/').pop();
    const storageRef = storage().ref(`${id}/${fileName}`);
    const task = storageRef.putFile(fileUri);
    task.on('state_changed', taskSnapshot => {
        console.log(`${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`);
    });
    task.catch((e) => {
        clog(e);
    })
    return task.then(async () => {
        let imageRef = storage().ref(`${id}/${fileName}`);
        return await imageRef.getDownloadURL();
    });
}
