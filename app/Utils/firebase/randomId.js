import {firebase} from "@react-native-firebase/auth";

export default function randomId(collection) {
    return firebase.firestore().collection(collection).doc().id;
}
