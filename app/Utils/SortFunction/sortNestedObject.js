// sortNestedObject(sortNestedObject[i]["data"], 'is_confirmed', '_id', true)
export default function sortNestedObject(arr, objectName, field, reverse) {
    if (reverse === true) {
        return arr.sort(sortReverseObject(objectName, field));
    } else {
        return arr.sort(sortNormalObject(objectName, field));
    }
}

function sortNormalObject(objectName, field) {
    return function (a, b) {
        let valueA = simplePropertyRetriever(a, objectName, field);
        let valueB = simplePropertyRetriever(b, objectName, field);

        if (valueA < valueB) {
            return 1;
        } else if (valueA > valueB) {
            return -1;
        } else {
            return 0;
        }
    }
}

function sortReverseObject(objectName, field) {
    return function (a, b) {
        let valueA = simplePropertyRetriever(a, objectName, field);
        let valueB = simplePropertyRetriever(b, objectName, field);

        if (valueA < valueB) {
            return -1;
        } else if (valueA > valueB) {
            return 1;
        } else {
            return 0;
        }
    }
}

function simplePropertyRetriever(obj, objectName, field) {
    return obj[objectName]?.[field] ?? '';
}
