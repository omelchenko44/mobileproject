// fieldSort(sortedArray[i]["data"], ['is_confirmed'])
export default function fieldSort(arr, fields, reverse) {

    if (reverse === true) {
        return arr.slice().sort(function (a, b) {
            return fields.reduce(function (c, d, e, f) {
                let _a = a[d] || 0, _b = b[d] || 0
                return c + (f.length - e) * ((typeof _a === 'string' ? _a.toLocaleLowerCase() : _a) > (typeof _b === 'string' ? _b.toLocaleLowerCase() : _b) ? -1 : (typeof _a === 'string' ? _a.toLocaleLowerCase() : _a) < (typeof _b === 'string' ? _b.toLocaleLowerCase() : _b) ? 1 : 0)
            }, 0)
        })
    } else {
        return arr.slice().sort(function (a, b) {
            return fields.reduce(function (c, d, e, f) {
                let _a = a[d] || 0, _b = b[d] || 0
                return c + (f.length - e) * ((typeof _a === 'string' ? _a.toLocaleLowerCase() : _a) > (typeof _b === 'string' ? _b.toLocaleLowerCase() : _b) ? 1 : (typeof _a === 'string' ? _a.toLocaleLowerCase() : _a) < (typeof _b === 'string' ? _b.toLocaleLowerCase() : _b) ? -1 : 0)
            }, 0)
        })
    }
}
