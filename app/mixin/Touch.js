import React, {Component} from 'react';
import {State, TapGestureHandler} from 'react-native-gesture-handler';

export default function Touch(Class) {
    class Touchable extends Component {
        onState = ({nativeEvent: e}) => {
            var {onPress} = this.props;

            if (e.state === State.ACTIVE && onPress) {
                onPress(e);
            }
        };

        render() {
            return (
                <TapGestureHandler onHandlerStateChange={this.onState}>
                    <Class {...this.props}/>
                </TapGestureHandler>
            );
        }
    }

    return Touchable;
}
