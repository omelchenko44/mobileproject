import React, {useContext, useEffect, useRef, useState,} from "react";
import styled from "styled-components";
import {SafeAreaView, Image, Text, View, TouchableOpacity, Dimensions} from "react-native";
import {AuthContext} from "../../firebaseAuth/AuthProvider";
import i18n from "../../locales/index";
import {TabBar, TabView} from "react-native-tab-view";
import {MyPurchases} from "./myPurchases";
import {MySales} from "./mySales";
import {AboutMe} from "./aboutMe";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import RBSheet from "react-native-raw-bottom-sheet";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {launchImageLibrary} from "react-native-image-picker";
import {Camera} from "../../components/Camera";
import firestore from "@react-native-firebase/firestore";

let {t} = i18n;
const routes = [
    {key: "aboutMe", title: t("aboutMe")},
    {key: "mySales", title: t("mySales")},
    {key: "myPurchases", title: t("myPurchases")},
];
export const ProfileScreen = ({route, navigation}) => {
    const [user, setUser] = useState(route?.params?.user ?? {});
    const [index, setIndex] = useState(0);
    const [photoUri, setPhotoUri] = useState('');
    const [showUserPhoto, setShowUserPhoto] = useState(true);
    const refRBSheet = useRef();
    const {logout} = useContext(AuthContext);

    useEffect(() => {
        getUsersData().then(() => {
        });
    }, []);

    async function getUsersData() {
        const userDocument = await firestore().collection('users')
            .where("uid", "==", user.uid)
            .get();
        userDocument.docs.map((user) => {
            setShowUserPhoto(false)
            setUser(user);
            setTimeout(() => {
                setShowUserPhoto(true)
            }, 10);
        })
    }

    function onLogout() {
        logout(user['uid'])
    }

    async function choosePhotoFromGallery() {
        let options = {mediaType: 'photo', selectionLimit: 1};
        const result = await launchImageLibrary(options);
        if (result.didCancel !== true) {
            setPhotoUri(result.assets[0]['uri']);
            closeRBSheet();
        }
    }

    function closeRBSheet() {
        refRBSheet.current.close();
    }

    function openCamera() {
        closeRBSheet();
        navigation.navigate("Camera", {onMakePhoto: onMakePhoto});
    }

    function onMakePhoto(photo) {
        setPhotoUri(photo.uri);
    }


    const renderScene = ({route}) => {
        switch (route.key) {
            case "aboutMe":
                return (<AboutMe photoUri={photoUri} user={user}/>);
            case "mySales":
                return (<MySales/>);
            case "myPurchases":
                return (<MyPurchases/>);
        }
    }

    return (
        <Container>
            <View style={{alignItems: 'center', marginTop: 10, marginBottom: 20}}>
                <Text style={{fontWeight: 'bold', fontSize: 17}}>Account</Text>
                <TouchableOpacity
                    onPress={onLogout}
                    style={{position: 'absolute', right: 10, top: -3}}>
                    <MaterialIcons name={'logout'} size={27}/>
                </TouchableOpacity>
            </View>
            <View style={{
                height: 100,
                paddingLeft: 24,
                paddingRight: 24,
                alignItems: "center",
                flexDirection: "column"
            }}>
                <View>
                    {(photoUri !== '' || user.photoURL !== '') && showUserPhoto ?
                        <Image source={{uri: photoUri !== '' ? photoUri : user.photoURL}}
                               style={{width: 70, height: 70, borderRadius: 50, marginBottom: 10}}/>
                        :
                        <View style={{
                            width: 70,
                            height: 70,
                            borderRadius: 50,
                            marginBottom: 10,
                            backgroundColor: 'silver'
                        }}/>
                    }
                    <TouchableOpacity style={{position: 'absolute', right: -10, bottom: 5}}
                                      onPress={() => {
                                          refRBSheet.current.open();
                                      }}>
                        <MaterialCommunityIcons name={"circle-edit-outline"} size={20}/>
                    </TouchableOpacity>
                </View>
                <View style={{paddingLeft: 16}}>
                    <Text style={{fontSize: 15}}>{user['displayName'] ?? user['email']}</Text>
                </View>
            </View>
            <TabView
                keyboardDismissMode={"none"}
                navigationState={{index, routes}}
                renderScene={renderScene}
                onIndexChange={index => setIndex(index)}
                initialLayout={{width: Dimensions.get("window").width}}
                renderTabBar={props =>
                    <TabBar
                        {...props}
                        indicatorStyle={{backgroundColor: "silver"}}
                        style={{backgroundColor: "white"}}
                        activeColor={"black"}
                        inactiveColor={"silver"}
                        inactiveOpacity={0.5}
                        activeOpacity={1.0}
                        keyboardDismissMode={"none"}
                    />
                }
            />
            <RBSheet
                ref={refRBSheet}
                closeOnPressMask={true}
                closeOnPressBack={true}
                closeOnDragDown={true}
                height={185}
                openDuration={400}
                customStyles={{
                    container: {
                        borderRadius: 10,
                        width: '97%',
                        marginBottom: 20
                    },
                    wrapper: {
                        justifyContent: "center",
                        alignItems: "center",
                    }
                }}>
                <View style={{borderWidth: 1, borderColor: 'gray'}}/>
                <BottomSheetButton onPress={choosePhotoFromGallery}>
                    <BottomSheetText>{t('changePhoto')}</BottomSheetText>
                </BottomSheetButton>
                <BottomSheetButton onPress={openCamera}>
                    <BottomSheetText>{t('newPhoto')}</BottomSheetText>
                </BottomSheetButton>
                <BottomSheetButton onPress={() => {
                    setPhotoUri('');
                }}>
                    <BottomSheetText style={{color: "red"}}> {t('deletePhoto')}</BottomSheetText>
                </BottomSheetButton>
            </RBSheet>
        </Container>
    );
};


const Container = styled(SafeAreaView)`
  flex: 1;
  background-color: #F8F8F8;
`;

const BottomSheetText = styled(Text)`
  color: #000000;
  text-align: left;
  margin-left: 10px;
`;

const BottomSheetButton = styled(TouchableOpacity)`
  height: 50px;
  justify-content: center;
`;
