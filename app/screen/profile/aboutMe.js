import React, {useEffect, useState} from "react";
import firestore from "@react-native-firebase/firestore";
import {ActivityIndicator, Text, TextInput, TouchableOpacity, View} from "react-native";
import SearchableSelect from "../../components/Select/searchableSelect";
import i18n from "../../locales/index";
import Job from "../../services/Job";

import {districtActions} from "../../storage/realm";
import fieldSort from "../../Utils/SortFunction/fieldSort";
import styled from "styled-components";
import {firebase} from "@react-native-firebase/firestore";
import clog from "../../Utils/Functions/clog";
import MyAwesomeAlert from "../../components/AwesomeAlert";
import uploadPhotoToFirebase from "../../Utils/firebase/uploadPhotoToFirebase";

const {t} = i18n
const JobSync = new Job();
export const AboutMe = ({photoUri, user}) => {
    const [districtList, setDistrictList] = useState(fieldSort(districtActions.getAllDistricts(), ['name']));
    const [selectedDistrictName, setSelectedDistrictName] = useState(user.selectedDistrictName ? user.selectedDistrictName : districtList[0].name);
    const [selectedDistrictId, setSelectedDistrictId] = useState(user.selectedDistrictId ? user.selectedDistrictId : districtList[0]._id);
    const [description, setDescription] = useState(user.description ? user.description : '');
    const [email, setEmail] = useState(user.email ? user.email : '');
    const [viber, setViber] = useState(user.viber ? user.viber : '');
    const [telegram, setTelegram] = useState(user.telegram ? user.telegram : '');
    const [number, setNumber] = useState(user.number ? user.number : '');
    const [loader, setLoader] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    useEffect(() => {
        getDistrictFromApi().then(() => {
        });
    }, []);

    function getDistrictFromApi() {
        return JobSync.getDistrict().then(() => {
            let districtRealm = fieldSort(districtActions.getAllDistricts(), ['name']);
            setDistrictList(districtRealm);
        }).catch((e) => {
            return e;
        });
    }

    async function saveUserData() {
        setLoader(true);
        let user = firebase.auth().currentUser;
        let photoURLFirebase = '';
        if (photoUri !== "") {
            photoURLFirebase = await uploadPhotoToFirebase(photoUri, user.uid);
        }

        let newUserObject = {
            uid: user.uid,
            selectedDistrictName: selectedDistrictName,
            selectedDistrictId: selectedDistrictId,
            description: description,
            email: email,
            viber: viber,
            telegram: telegram,
            number: number,
            photoURL: photoURLFirebase
        }

        firestore()
            .collection('users')
            .doc(user.uid)
            .set(newUserObject)
            .then(() => {
                console.log('User data updated!');
                setLoader(false);
            })
            .catch((e) => {
                clog(e)
                setShowAlert(true);
                setErrorMessage(t('serverError'));
            });
    }

    function hideAlert() {
        setShowAlert(false);
    }

    return (
        <Container>
            <DataContainer>
                <DataText>{t('region')}</DataText>
                <SelectContainer>
                    <SearchableSelect
                        dataSource={districtList}
                        selectedLabel={selectedDistrictName}
                        placeholder={t('selectRegion')}
                        searchBarPlaceHolder={t('searchRegion')}
                        pickerTitle={t('selectRegion')}
                        close={true}
                        selectedValue={(newDistrict) => {
                            if (newDistrict) {
                                setSelectedDistrictId(newDistrict._id);
                                setSelectedDistrictName(newDistrict.name);
                            } else {
                                setSelectedDistrictId(null);
                                setSelectedDistrictName('');
                            }
                        }}/>
                </SelectContainer>
            </DataContainer>
            <InputDataContainer>
                <BoldText>{t('email')}</BoldText>
                <MyTextInput
                    keyboardType={'email-address'}
                    value={email}
                    onChangeText={(email) => {
                        setEmail(email);
                    }}
                    placeholder={"example@gmail.com"}
                    returnKeyType={'done'}
                />
            </InputDataContainer>
            <InputDataContainer>
                <BoldText>{t('phone')}</BoldText>
                <MyTextInput
                    value={number}
                    onChangeText={(number) => {
                        setNumber(number);
                    }}
                    placeholder={"+380977770668"}
                    keyboardType="phone-pad"
                    returnKeyType={'done'}
                />
            </InputDataContainer>
            <InputDataContainer>
                <BoldText>{t('telegram')}</BoldText>
                <MyTextInput
                    value={telegram}
                    onChangeText={(telegram) => {
                        setTelegram(telegram);
                    }}
                    placeholder={"@example"}
                    returnKeyType={'done'}
                />
            </InputDataContainer>
            <InputDataContainer>
                <BoldText>{t('viber')}</BoldText>
                <MyTextInput
                    value={viber}
                    onChangeText={(viber) => {
                        setViber(viber);
                    }}
                    placeholder={"+380984441097"}
                    keyboardType={"phone-pad"}
                    returnKeyType={'done'}
                />
            </InputDataContainer>
            <InputDataContainer>
                <BoldText>{t('description')}</BoldText>
                <MyTextInput
                    value={description}
                    onChangeText={(description) => {
                        setDescription(description)
                    }}
                    placeholder={t("exampleDescription")}
                />
            </InputDataContainer>
            <SaveButtonContainer>
                <SaveButton
                    onPress={async () => {
                        await saveUserData();
                    }}>
                    {loader === true ?
                        <ActivityIndicator size="large"/>
                        :
                        <Text style={{color: 'white', fontSize: 18}}> {t('saveInfo')}</Text>
                    }
                </SaveButton>
            </SaveButtonContainer>
            <MyAwesomeAlert
                showAlert={showAlert}
                title={t('warning')}
                errorMessage={errorMessage}
                showConfirmButton={false}
                cancelText={t('cancel')}
                hideAlert={hideAlert}
                titleStyle={{color: '#ffba00'}}
            />
        </Container>
    )
};


const Container = styled(View)`
  padding: 10px 10px;
`;

const MyTextInput = styled(TextInput)`
  height: 40px;
  font-size: 15px;
  width: 300px;
  padding: 10px;
  margin-left: 15px;
`;

const DataContainer = styled(View)`
  flex-direction: row;
  align-items: center;
  height: 60px;
`;

const InputDataContainer = styled(View)`
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;
  border-bottom-width: 1px;
  border-bottom-color: #b3b3b3;
  margin-bottom: 10px;
`;

const SelectContainer = styled(View)`
  height: 60px;
  margin-left: 35px;
  width: 79%;
`;

const DataText = styled(Text)`
  font-weight: bold;
  padding-bottom: 30px;
`;

const BoldText = styled(Text)`
  font-weight: bold;
  margin-bottom: 10px;
`;

const SaveButtonContainer = styled(View)`
  align-items: center;
  margin-top: 50px;
`;

const SaveButton = styled(TouchableOpacity)`
  width: 190px;
  height: 45px;
  background-color: dodgerblue;
  border-radius: 10px;
  align-items: center;
  justify-content: center;
`;
