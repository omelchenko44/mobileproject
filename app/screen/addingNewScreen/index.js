import React, {useEffect} from "react";
import styled from "styled-components";
import {Text, SafeAreaView} from "react-native";

export const AddNewScreen = () => {

    useEffect(() => {
    })

    return (
        <Container>
            <Text>AddNew screen</Text>
        </Container>
    );
}

const Container = styled(SafeAreaView)`
  flex: 1;
`;
