import React, {useState} from "react";
import {Dimensions, Image, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import Fontisto from "react-native-vector-icons/Fontisto";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import i18n from "../../locales/index";

const {t} = i18n;
export const DetailCorporationName = ({props}) => {
    const [item] = useState(props.route?.params?.item ?? {})

    function getSeller(name) {
        console.log(name);
    }

    let {navigation} = props;
    return (
        <SafeAreaView style={{backgroundColor: "#EBEBEB", height: Dimensions.get("window").height}}>
            <View style={{
                height: 60,
                justifyContent: "space-between",
                flexDirection: "row",
                paddingLeft: 24,
                alignItems: "center",
                backgroundColor: 'white'

            }}>
                <TouchableOpacity onPress={() => {
                    navigation.goBack();
                }}>
                    <MaterialIcons name={"arrow-back-ios"} size={28}/>
                </TouchableOpacity>
                <Text style={{right: 24, fontSize: 20, fontWeight: "bold"}}>{item.name}</Text>
                <Text/>
            </View>

            <View style={{height: 20, backgroundColor: "#EBEBEB"}}/>

            <View style={{backgroundColor: 'white'}}>
                <View style={{flexDirection: "row", paddingLeft: 24, alignItems: "center", height: 60}}>
                    <Image source={item.path} style={{width: 50, height: 50, borderRadius: 50}}/>

                    <Text style={{paddingLeft: 24, fontSize: 20}}>{item.name}</Text>
                </View>
                <View style={{paddingLeft: 24, paddingRight: 24, paddingBottom: 10}}>
                    <Text>{item.description}</Text>
                </View>
                <View style={{height: 20, backgroundColor: "white"}}/>
            </View>

            <View style={{height: 20, backgroundColor: "#EBEBEB"}}/>
            <View style={{
                paddingLeft: 24,
                paddingRight: 24,
                backgroundColor: 'white',
                paddingBottom: 24,
                paddingTop: 10
            }}>
                <Text style={{fontWeight: "bold", fontSize: 16, top: 10}}>{t('sellersContacts')}</Text>
                <TouchableOpacity onPress={() => {
                    getSeller("telegram");
                }} style={{flexDirection: "column", justifyContent: "space-between", paddingTop: 24}}>
                    <View style={{
                        borderRadius: 8,
                        height: 44,
                        alignItems: "center",
                        borderColor: "#007AFF",
                        borderWidth: 1,
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }}>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <EvilIcons name={"sc-telegram"} size={35} style={{paddingLeft: 19}}/>
                            <Text style={{paddingLeft: 19}}>Telegram</Text>
                        </View>
                        <FontAwesome5 name={"external-link-alt"} size={18} style={{paddingRight: 10}}/>
                    </View>

                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    getSeller("viber");
                }} style={{flexDirection: "column", justifyContent: "space-between", paddingTop: 24}}>
                    <View style={{
                        borderRadius: 8,
                        height: 44,
                        alignItems: "center",
                        borderColor: "#007AFF",
                        borderWidth: 1,
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }}>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <Fontisto name={"viber"} size={30} style={{paddingLeft: 19}}/>
                            <Text style={{paddingLeft: 25}}>Viber</Text>
                        </View>
                        <FontAwesome5 name={"external-link-alt"} size={18} style={{paddingRight: 10}}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    getSeller("phone");
                }} style={{flexDirection: "column", justifyContent: "space-between", paddingTop: 24}}>
                    <View style={{
                        borderRadius: 8,
                        height: 44,
                        alignItems: "center",
                        borderColor: "#007AFF",
                        borderWidth: 1,
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }}>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <Feather name={"phone-call"} size={30} style={{paddingLeft: 19}}/>
                            <Text style={{paddingLeft: 25}}>Phone</Text>
                        </View>
                        <FontAwesome5 name={"external-link-alt"} size={18} style={{paddingRight: 10}}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    getSeller("mail");
                }} style={{flexDirection: "column", justifyContent: "space-between", paddingTop: 24}}>
                    <View style={{
                        borderRadius: 8,
                        height: 44,
                        alignItems: "center",
                        borderColor: "#007AFF",
                        borderWidth: 1,
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }}>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <Feather name={"mail"} size={30} style={{paddingLeft: 19}}/>
                            <Text style={{paddingLeft: 25}}>Mail</Text>
                        </View>
                        <FontAwesome5 name={"external-link-alt"} size={18} style={{paddingRight: 10}}/>
                    </View>
                </TouchableOpacity>
            </View>


        </SafeAreaView>
    );

}
