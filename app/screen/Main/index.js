import React from "react";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import Feather from "react-native-vector-icons/Feather";
import {HomeNavigation} from "../home/homeNavigation";
import {AddNewScreen} from "../addingNewScreen";
import {ProfileScreen} from "../profile";

const Tab = createBottomTabNavigator();

export const Main = ({route}) => {
    const {user} = route.params;
    return (
        <Tab.Navigator initialRouteName="HomeScreen"
                       tabBarOptions={{activeTintColor: "dodgerblue", activeBackgroundColor: "whitesmoke"}}>
            <Tab.Screen name={"HomeScreen"}
                        component={HomeNavigation}
                        options={{
                            tabBarIcon: ({color}) => (
                                <SimpleLineIcons name={"home"} color={color} size={30} style={{top: 5}}/>),
                            tabBarLabel: "",
                        }}/>
            <Tab.Screen name={"AddingScreen"}
                        component={AddNewScreen}
                        options={{
                            tabBarIcon: ({color}) => (
                                <Feather name={"plus-circle"} color={color} size={30} style={{top: 5}}/>),
                            tabBarLabel: "",
                        }}/>
            <Tab.Screen name={"ProfileScreen"}
                        component={ProfileScreen}
                        options={{
                            tabBarIcon: ({color}) => (
                                <Feather name={"user"} color={color} size={30} style={{top: 5}}/>),
                            tabBarLabel: "",
                        }}
                        initialParams={{user: user}}/>
        </Tab.Navigator>
    )
}
