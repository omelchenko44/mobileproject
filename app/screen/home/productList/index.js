import React, {useEffect, useState} from "react";
import styled from "styled-components";
import {Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import i18n from "../../../locales/index";

const {t} = i18n;
export const ProductList = ({navigation, route}) => {
    const [name] = useState(route?.params?.name ?? "");

    useEffect(() => {

    })

    function goToFilter() {
        let {navigation} = props;
        navigation.navigate('FilterScreen')
    }

    return (
        <Container>
            <View style={{
                height: 60,
                justifyContent: "space-between",
                flexDirection: "row",
                paddingLeft: 24,
                alignItems: "center",
            }}>
                <TouchableOpacity onPress={() => {
                    navigation.goBack();
                }}>
                    <MaterialIcons name={"arrow-back-ios"} size={28}/>
                </TouchableOpacity>
                <Text style={{right: 24, fontSize: 20, fontWeight: "bold"}}>{name}</Text>
                <Text/>
            </View>

            <ScrollView>
                <View style={{flexDirection: "row", flexWrap: "wrap", justifyContent: "center"}}>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                </View>
                <View style={{height: 120}}/>
            </ScrollView>
            <TouchableOpacity onPress={() => {
                goToFilter();
            }} style={{
                position: "absolute",
                height: 34,
                width: 84,
                bottom: 80,
                left: Dimensions.get("screen").width * 0.4,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#ffffff",
                borderRadius: 4,
            }}>
                <MaterialCommunityIcons name={"filter-variant"} size={30}/>
                <Text>{t('filter')}</Text>
            </TouchableOpacity>
        </Container>
    );
}

const Container = styled(SafeAreaView)`
`;
