import React from "react";
import styled from "styled-components";
import {Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import i18n from "../../locales/index";

const {t} = i18n;
export const HomeScreen = ({props}) => {

    function goToList(type) {
        let {navigation} = props;
        console.log(navigation)
        navigation.navigate("ProductListScreen", {name: type});
    }

    return (
        <Container>
            <View style={{height: 100, flexDirection: "column", justifyContent: "center", padding: 24}}>
                <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={{fontSize: 20, fontWeight: "bold"}}>{t('appName')}</Text>
                    <AntDesign name={"search1"} size={28}/>
                </View>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{top: 10}}>
                    <View style={{borderRadius: 17, backgroundColor: "#F2F2F2"}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('catalog')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('man')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('woman')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('children')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>Beauty</Text>
                    </View>
                </ScrollView>
            </View>

            <View style={{paddingLeft: 24, paddingRight: 24, paddingTop: 10}}>
                <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={{fontSize: 20}}>VIP</Text>
                    <TouchableOpacity onPress={() => {
                        goToList("VIP");
                    }}>
                        <MaterialIcons name={"arrow-forward-ios"} size={28}/>
                    </TouchableOpacity>
                </View>
                <ScrollView horizontal={true}>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                </ScrollView>
            </View>

            <View style={{paddingLeft: 24, paddingRight: 24, paddingTop: 10}}>
                <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={{fontSize: 20}}>{t('free')}</Text>
                    <TouchableOpacity onPress={() => {
                        goToList("Free");
                    }}>
                        <MaterialIcons name={"arrow-forward-ios"} size={28}/>
                    </TouchableOpacity>
                </View>
                <ScrollView horizontal={true}>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                    <View style={{marginBottom: 10, margin: 10}}>
                        <Image source={require("../../assets/test-t-short.png")} style={{width: 154, height: 145}}/>
                        <Text style={{fontSize: 16, fontWeight: "bold", marginBottom: 3}}>24грн</Text>
                        <Text style={{fontSize: 16}}>T-shirt</Text>
                    </View>
                </ScrollView>
            </View>
        </Container>
    );
}

const Container = styled(SafeAreaView)`
  flex: 1;
  background-color: white;
`;

