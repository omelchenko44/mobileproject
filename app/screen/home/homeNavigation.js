import React from "react";
import {HomeScreen} from "../home/index";
import {ProductList} from "./productList/index";
import {FilterScreen} from "./filter/index";

import {createStackNavigator} from "@react-navigation/stack";

const Stack = createStackNavigator();
export const HomeNavigation = () => {
    return (
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="Home" component={HomeScreen}
                          initialParams={{navigation: this.props}}
                          options={{headerShown: false}}/>
            <Stack.Screen name="ProductListScreen" component={ProductList}
                          options={{headerShown: false}}/>
            <Stack.Screen name="FilterScreen" component={FilterScreen}
                          options={{headerShown: false}}/>
        </Stack.Navigator>
    );
}
