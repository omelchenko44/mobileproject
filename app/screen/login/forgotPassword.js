import React, {useState} from "react";

import {
    KeyboardAvoidingView,
    Platform, SafeAreaView,
    StyleSheet,
    Text,
    TextInput, TouchableOpacity,
    View,
} from "react-native";
import i18n from "../../locales/index";

const {t} = i18n;
export const ForgotPassword = () => {
    const [email, setEmail] = useState("");

    function onLogin() {
    }

    return (
        <SafeAreaView style={{justifyContent: 'space-between', flex: 1}}>
            <View>
                <View style={{alignItems: "center", marginTop: 72}}>
                    <Text style={{fontSize: 26, fontWeight: "bold"}}>{t('appName')}</Text>
                </View>
                <View style={{alignItems: "center", marginTop: 50}}>
                    <Text style={{fontSize: 24}}>{t('forgotPassword')}</Text>
                </View>
            </View>

            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 0}
                style={{}}>
                <View style={{marginTop: 65, marginLeft: 24, width: '100%'}}>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        placeholder={"Email"}
                        placeholderTextColor={'gray'}
                        style={styles.input}
                    />
                </View>
            </KeyboardAvoidingView>
            <View style={{width: '100%', justifyContent: "center", alignItems: "center"}}>
                <TouchableOpacity
                    onPress={() => {
                        onLogin();
                    }}
                    style={{
                        width: "86%",
                        backgroundColor: 'dodgerblue',
                        marginRight: 24,
                        marginLeft: 24,
                        justifyContent: "center",
                        alignItems: "center",
                        height: 50,
                        borderRadius: 4,
                        marginBottom: 20
                    }}>
                    <Text style={{fontSize: 17, color: 'white'}}>{t('continue')}</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    input: {
        paddingTop: 10,
        marginRight: 34,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: "silver",
        marginBottom: 30,
        borderRadius: 7,
        flexDirection: "row",
        alignItems: "center"
    },
    inputContainer: {
        width: "90%",
        padding: 5,
        borderBottomWidth: 1,
        borderColor: "silver",
        marginBottom: 30,
        borderRadius: 7,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    buttonContainer: {
        backgroundColor: "#007AFF",
        paddingVertical: 15,
        width: "90%",
        borderRadius: 14,
        top: Platform.OS === "ios" ? 150 : 250,
    },
    buttonText: {
        color: "#fff",
        textAlign: "center",
        fontWeight: "700",
    },
    backgroundImage: {
        flex: 1
    },
    searchSection: {
        backgroundColor: "#fff",
        borderRadius: 50,
        marginLeft: 35,
        width: 340,
        height: 40,
        margin: 25,
    },
});

