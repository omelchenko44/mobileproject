import React, {useContext, useEffect, useState} from "react";
import {
    Image,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import styled from 'styled-components';
import auth from '@react-native-firebase/auth';
import clog from "../../Utils/Functions/clog";
import {AuthContext} from "../../firebaseAuth/AuthProvider";
import i18n from "i18n-js";
import Ionicons from "react-native-vector-icons/Ionicons";
import MyAwesomeAlert from "../../components/AwesomeAlert";
import {GoogleSignin} from '@react-native-google-signin/google-signin';

const {t} = i18n;
export const SignIn = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordClosed, setPasswordClosed] = useState(true);
    const [iconName, setIconName] = useState('eye-off');
    const [initializing, setInitializing] = useState(true);
    const [showAlert, setShowAlert] = useState( false);
    const [errorMessage, setErrorMessage] = useState('');

    const {login, loginWithGoogle} = useContext(AuthContext);

    useEffect(() => {
        GoogleSignin.configure({webClientId: '190378611857-udajrfv20e326eac0ctalnuu0orlss8k.apps.googleusercontent.com'});
        return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
    }, []);

    function onAuthStateChanged() {
        if (initializing) setInitializing(false);
    }

    async function _signIn() {
        const {idToken} = await GoogleSignin.signIn();
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        return loginWithGoogle(googleCredential);
    }


    function goToCreateAccountScreen() {
        navigation.navigate('CreateAccount')
    }

    function onLogin() {
        if (email === '') {
            showingAlert(t('pleaseEnterEmail'));
            return;
        }
        if (password === '') {
            showAlert(t('pleaseEnterPassword'));
            return;
        }
        try {
            login(email.toString().trim(), password);
        } catch (e) {
            clog(e)
            clog('e')
        }
    }

    function changePasswordIcon() {
        if (passwordClosed === true) {
            setPasswordClosed(false);
            setIconName('eye');
        } else {
            setPasswordClosed(true);
            setIconName('eye-off');
        }
    }

    function hideAlert() {
        setShowAlert(!showAlert)
    }

    function showingAlert(message) {
        setShowAlert(true);
        setErrorMessage(message);
    }

    return (
        <Container>
            <View>
                <AppNameContainer>
                    <Text style={{fontSize: 26, fontWeight: "bold"}}>{t('appName')}</Text>
                </AppNameContainer>
                <LoginContainer>
                    <Text style={{fontSize: 24}}>{t('login')}</Text>
                </LoginContainer>
            </View>

            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 0}>
                <View style={{marginTop: 15, marginLeft: 24, width: '100%'}}>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        placeholder={t('email')}
                        placeholderTextColor={'gray'}
                        style={styles.input}
                        keyboardType={'email-address'}
                    />
                </View>

                <View style={{flexDirection: "row"}}>
                    <View style={{marginTop: 15, marginLeft: 24, width: '100%'}}>
                        <TextInput
                            value={password}
                            onChangeText={(password) => setPassword(password)}
                            placeholder={t('password')}
                            placeholderTextColor={'gray'}
                            style={styles.input}
                            secureTextEntry={passwordClosed}
                        />
                    </View>
                    <EyeButton
                        onPress={() => {
                            changePasswordIcon()
                        }}>
                        <Ionicons name={iconName} size={25}/>
                    </EyeButton>
                    <Text style={{color: 'dodgerblue'}}>Forgot password?</Text>
                </View>
            </KeyboardAvoidingView>

            <ButtonsContainer>
                <TouchableOpacity
                    onPress={() => {
                        onLogin();
                    }}
                    style={styles.signInButton}>
                    <Text style={{fontSize: 17, color: 'white'}}>{t('signIn')}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.loginWithGoogle}
                    onPress={async () => {
                        await _signIn();
                    }}>
                    <GoogleImage
                        source={require('../../assets/google.png')}
                        style={{
                            height: 50,
                            width: 50,
                            left: 10,
                            position: 'absolute'
                        }}/>
                    <Text style={{fontSize: 17, marginLeft: 25, fontWeight: 'bold'}}>Sign in with Google</Text>
                </TouchableOpacity>


                <BottomContainer>
                    <Text style={{fontSize: 15}}>{t('dontHaveAccount')}?</Text>
                    <TouchableOpacity onPress={() => {
                        goToCreateAccountScreen();
                    }}>
                        <Text style={{color: 'dodgerblue'}}> {t('createAccount')}</Text>
                    </TouchableOpacity>
                </BottomContainer>
            </ButtonsContainer>
            <MyAwesomeAlert showAlert={showAlert}
                            title={t('warning')}
                            errorMessage={errorMessage}
                            showConfirmButton={false}
                            cancelText={t('cancel')}
                            hideAlert={hideAlert}
                            navigation={navigation}
                            titleStyle={{color: '#ffba00'}}
            />
        </Container>
    );
}

const styles = StyleSheet.create({
    input: {
        paddingTop: 10,
        marginRight: 34,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: "silver",
        marginBottom: 30,
        borderRadius: 7,
        flexDirection: "row",
        alignItems: "center"
    },
    signInButton: {
        width: "86%",
        backgroundColor: 'dodgerblue',
        marginRight: 24,
        marginLeft: 24,
        justifyContent: "center",
        alignItems: "center",
        height: 50,
        borderRadius: 4,
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 6, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 5,
    },
    loginWithGoogle: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        shadowColor: "#000",
        borderRadius: 4,
        shadowOffset: {
            width: 6, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 5,
        width: "86%",
        marginRight: 24,
        marginLeft: 24,
        marginTop: 10,
        height: 50,
    }
});

const Container = styled(SafeAreaView)`
  justify-content: space-between;
  flex: 1;
`;

const AppNameContainer = styled(View)`
  align-items: center;
  margin-top: 72px;
`;

const LoginContainer = styled(View)`
  align-items: center;
  margin-top: 50px;
`;

const GoogleImage = styled(Image)`
  height: 50px;
  width: 50px;
  margin-left: 10px;
`;

const ButtonsContainer = styled(View)`
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const EyeButton = styled(TouchableOpacity)`
  position: absolute;
  right: 25px;
  top: 25px;
`;

const BottomContainer = styled(View)`
  margin-bottom: 20px;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  margin-top: 10px;
`;
