import React, {useContext, useState} from "react";
import {
    KeyboardAvoidingView,
    Platform, SafeAreaView,
    StyleSheet,
    Text,
    TextInput, TouchableOpacity,
    View,
} from "react-native";
import i18n from '../../locales/index'
import clog from "../../Utils/Functions/clog";
import MyAwesomeAlert from "../../components/AwesomeAlert";
import {AuthContext} from "../../firebaseAuth/AuthProvider";
import Ionicons from "react-native-vector-icons/Ionicons";

const {t} = i18n;
export const CreateAccount = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConf, setPasswordConf] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [passwordClosed, setPasswordClosed] = useState(true);
    const [passwordConfClosed, setPasswordConfClosed] = useState(true);
    const [iconName, setIconName] = useState('eye-off');
    const [iconConfName, setIconConfName] = useState('eye-off');

    const {register} = useContext(AuthContext);

    function registerAccount() {
        if (email === '') {
            showingAlert(t('pleaseEnterEmail'));
            return;
        }
        if (validate(email) === false) {
            showingAlert(t('pleaseEnterValidEmail'));
            return;
        }
        if (password === '') {
            showingAlert(t('pleaseEnterPassword'));
            return;
        }
        if (password !== passwordConf) {
            showingAlert(t('passwordNotConfirmed'));
            return;
        }
        try {
            register(email.toString().trim(), password, showingAlert);
        } catch (e) {
            clog(e)
            clog('e')
        }
    }

    function validate(email) {
        let reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w\w+)+$/;
        return reg.test(email)
    }

    function goToSignInScreen() {
        navigation.goBack();
    }

    function hideAlert() {
        setShowAlert(!showAlert)
    }

    function showingAlert(message) {
        setShowAlert(true);
        setErrorMessage(message);
    }

    function changePasswordIcon(type, state) {
        if (type === 'password') {
            setIconName(state === true ? 'eye-off' : 'eye');
            setPasswordClosed(state);
        } else {
            setIconConfName(state === true ? 'eye-off' : 'eye');
            setPasswordConfClosed(state);
        }
    }

    return (
        <SafeAreaView style={{justifyContent: 'space-between', flex: 1}}>
            <View>
                <View style={{alignItems: "center", marginTop: 72}}>
                    <Text style={{fontSize: 26, fontWeight: "bold"}}>{t('appName')}</Text>
                </View>
                <View style={{alignItems: "center", marginTop: 50}}>
                    <Text style={{fontSize: 24}}>{t('createAccount')}</Text>
                </View>
            </View>

            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 0}
                style={{}}>
                <View style={{marginLeft: 24, width: '100%'}}>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        placeholder={t("email")}
                        placeholderTextColor={'gray'}
                        style={styles.input}
                        keyboardType={'email-address'}
                    />
                </View>
                <View style={{flexDirection: "row"}}>
                    <View style={{marginLeft: 24, width: '100%'}}>
                        <TextInput
                            value={password}
                            onChangeText={(password) => setPassword(password)}
                            placeholder={t("password")}
                            placeholderTextColor={'gray'}
                            style={styles.input}
                            secureTextEntry={passwordClosed}
                        />
                        <TouchableOpacity
                            style={{position: 'absolute', right: 40, top: 10}}
                            onPress={() => {
                                changePasswordIcon('password', !passwordClosed)
                            }}>
                            <Ionicons name={iconName} size={25}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection: "row"}}>
                    <View style={{marginLeft: 24, width: '100%'}}>
                        <TextInput
                            value={passwordConf}
                            onChangeText={(passwordConf) => setPasswordConf(passwordConf)}
                            placeholder={t("confirmPassword")}
                            placeholderTextColor={'gray'}
                            style={styles.input}
                            secureTextEntry={passwordConfClosed}
                        />
                        <TouchableOpacity
                            style={{position: 'absolute', right: 40, top: 10}}
                            onPress={() => {
                                changePasswordIcon('passwordConf', !passwordConfClosed)
                            }}>
                            <Ionicons name={iconConfName} size={25}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>

            <View style={{width: '100%', justifyContent: "center", alignItems: "center"}}>
                <TouchableOpacity
                    onPress={() => {
                        registerAccount();
                    }}
                    style={{
                        width: "86%",
                        backgroundColor: 'dodgerblue',
                        marginRight: 24,
                        marginLeft: 24,
                        justifyContent: "center",
                        alignItems: "center",
                        height: 50,
                        borderRadius: 4,
                        marginBottom: 20
                    }}>
                    <Text style={{fontSize: 17, color: 'white'}}>{t('createAccount')}</Text>
                </TouchableOpacity>
                <View style={{marginBottom: 20, alignItems: "center", justifyContent: "center", flexDirection: "row"}}>
                    <Text style={{fontSize: 15}}>{t('alreadyHaveAccount')}?</Text>
                    <TouchableOpacity onPress={() => {
                        goToSignInScreen();
                    }}>
                        <Text style={{color: 'dodgerblue'}}> {t('signIn')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <MyAwesomeAlert showAlert={showAlert}
                            title={t('warning')}
                            errorMessage={errorMessage}
                            showConfirmButton={false}
                            cancelText={t('cancel')}
                            hideAlert={hideAlert}
                            navigation={navigation}
                            titleStyle={{color: '#ffba00'}}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    input: {
        paddingTop: 10,
        marginRight: 34,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: "silver",
        marginBottom: 30,
        borderRadius: 7,
        flexDirection: "row",
        alignItems: "center"
    },
});

