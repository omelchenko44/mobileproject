import React, {useState} from "react";
import {SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import i18n from "../../locales/index";

const {t} = i18n;

export const SearchScreen = ({navigation}) => {
    const [search, setSearch] = useState("");

    return (
        <SafeAreaView>
            <View style={{flexDirection: "column", padding: 24}}>
                <View style={{flexDirection: "row", alignItems: "center", paddingBottom: 20}}>
                    <TouchableOpacity onPress={() => {
                        navigation.goBack();
                    }}>
                        <MaterialIcons name={"arrow-back-ios"} size={28}/>
                    </TouchableOpacity>
                    {/*<CustomSearchBar*/}
                    {/*  placeholder={"Search"}*/}
                    {/*  updateSearch={updateSearch}*/}
                    {/*  value={search}*/}
                    {/*/>*/}
                </View>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{}}>
                    <View style={{borderRadius: 17, backgroundColor: "#F2F2F2"}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('catalog')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('man')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('woman')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>{t('children')}</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>Beauty</Text>
                    </View>
                    <View style={{backgroundColor: "#F2F2F2", marginLeft: 10, borderRadius: 17}}>
                        <Text style={{marginLeft: 10, marginRight: 10}}>House</Text>
                    </View>
                </ScrollView>
            </View>

        </SafeAreaView>
    );
}
