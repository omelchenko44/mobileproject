import React, {useState} from "react";
import { Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import i18n from "../../locales/index";

const {t} = i18n;
export const ProductDetailScreen = ({props}) => {
  const [item] = useState(props.route?.params?.item ?? {})
  function goToContact() {
    let { navigation } = props;
    navigation.navigate("ContactScreen");

  }
    let { navigation } = props;
    return (
      <SafeAreaView style={{ backgroundColor: "#ffffff" }}>
        <ScrollView>
          <View style={{ height: 375, backgroundColor: "#dedede" }}>
            <View style={{
              height: 60,
              justifyContent: "flex-start",
              flexDirection: "row",
              paddingLeft: 24,
              alignItems: "center",
            }}>
              <TouchableOpacity onPress={() => {
                navigation.goBack();
              }}>
                <MaterialIcons name={"arrow-back-ios"} size={28} />
              </TouchableOpacity>
            </View>
            <View style={{ alignItems: "center" }}>
              {item.newItem === true ?
                <Image
                  style={{ height: 350, width: 200, bottom: 45 }}
                  source={{ uri: `file://${item.path}` }}
                />
                :
                <Image source={item.path} style={{ width: 200, height: 350, bottom: 45 }} />
              }
            </View>
          </View>

          <View style={{ paddingLeft: 24, paddingRight: 24, paddingTop: 16 }}>
            <View>
              <Text style={{ fontSize: 18 }}>{item.name}</Text>
              <Text style={{ fontSize: 16 }}>{item.description}</Text>
            </View>
            <View style={{
              paddingTop: 10,
              backgroundColor: "white",
              flexDirection: "column",
              alignItems: "center",
            }}>
              <TouchableOpacity onPress={() => {
                goToContact();
              }}>
                <View style={{
                  borderRadius: 4,
                  backgroundColor: "#007AFF",
                  height: 40,
                  width: Dimensions.get("window").width * 0.8,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                  <Text style={{ color: "white", fontWeight: "bold" }}>{t('howToGetIt')}</Text>
                </View>
              </TouchableOpacity>
              <View style={{
                justifyContent: "space-between",
                flexDirection: "row",
                paddingTop: 24,
                paddingLeft: 24,
                paddingRight: 24,
                paddingBottom: 12,
                backgroundColor: "white",
                width: Dimensions.get("window").width,
              }}>
                <Text style={{ fontSize: 17 }}>{t('size')}</Text>
                <Text style={{ fontSize: 17, fontWeight: "bold" }}>{item.size}</Text>
              </View>
              <View style={{ height: 1, width: "100%", backgroundColor: "silver" }} />
              <View style={{
                justifyContent: "space-between",
                flexDirection: "row",
                paddingTop: 24,
                paddingLeft: 24,
                paddingRight: 24,
                paddingBottom: 12,
                backgroundColor: "white",
                width: Dimensions.get("window").width,
              }}>
                <Text style={{ fontSize: 17 }}>{t('city')}</Text>
                <Text style={{ fontSize: 17, fontWeight: "bold" }}>{item.city}</Text>
              </View>
              <View style={{ height: 1, width: "100%", backgroundColor: "silver" }} />
              <View style={{
                justifyContent: "space-between",
                flexDirection: "row",
                paddingTop: 24,
                paddingLeft: 24,
                paddingRight: 24,
                paddingBottom: 12,
                backgroundColor: "white",
                width: Dimensions.get("window").width,
              }}>
                <Text style={{ fontSize: 17 }}>{t('color')}</Text>
                <Text style={{ fontSize: 17, fontWeight: "bold" }}>{item.color}</Text>
              </View>
              <View style={{ height: 1, width: "100%", backgroundColor: "silver" }} />
              <View style={{
                justifyContent: "space-between",
                flexDirection: "row",
                paddingTop: 24,
                paddingLeft: 24,
                paddingRight: 24,
                paddingBottom: 12,
                backgroundColor: "white",
                width: Dimensions.get("window").width,
              }}>
                <Text style={{ fontSize: 17 }}>{t('price')}</Text>
                {item.price ?
                  <Text style={{fontSize: 17, fontWeight: "bold" }}>{item.price} UAH</Text>
                  :
                  <Text style={{ fontSize: 17, fontWeight: "bold" }}>{t('free')}</Text>
                }
              </View>
              <View style={{ height: 1, width: "100%", backgroundColor: "silver" }} />

              <View style={{
                justifyContent: "space-between",
                flexDirection: "row",
                paddingTop: 24,
                paddingLeft: 24,
                paddingRight: 24,
                paddingBottom: 12,
                backgroundColor: "white",
                width: Dimensions.get("window").width,
              }}>
                <Text style={{ fontSize: 17 }}>{t('lustUpdate')}</Text>
                <Text style={{ fontSize: 17, fontWeight: "bold" }}>03 July 2021</Text>
              </View>
              <View style={{ height: 1, width: "100%", backgroundColor: "silver" }} />
            </View>
          </View>
          <View style={{ height: 400 }} />
        </ScrollView>
      </SafeAreaView>
    );
}
