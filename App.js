import React from "react";
import {SafeAreaView, useColorScheme} from "react-native";

import {Colors} from "react-native/Libraries/NewAppScreen";
import {Navigation} from "./app/components/Navigation";
import RNBootSplash from "react-native-bootsplash";
import {AuthProvider} from "./app/firebaseAuth/AuthProvider";
import clog from "./app/Utils/Functions/clog";

const App: () => Node = () => {
    const isDarkMode = useColorScheme() === "dark";
    React.useEffect(() => {
        setTimeout(async () => {
            try {
                await RNBootSplash.hide({fade: true});
            } catch (e) {
                clog(e)
                clog('e')
            }
        }, 1000);
    }, []);


    return (
        <AuthProvider>
            <SafeAreaView style={{
                backgroundColor: isDarkMode ? Colors.lighter : Colors.lighter,
                flexDirection: "column",
                justifyContent: "flex-end",
                height: "100%",
            }}>
                <Navigation/>
            </SafeAreaView>
        </AuthProvider>
    );
};
export default App;
